@extends(backpack_view('blank'))


@section('header')
    {{ $header ?? '' }}
@endsection


@section('content')
    {{ $slot }}
@endsection
